#!/usr/bin/python3

import roulette 

players = []

while not players or roulette.is_adding_player():
    player = roulette.create_player()

    players.append(player)

revolver = roulette.create_revolver()

while revolver.count('shell'):
    for player in players:
        name = player['name'].lower().capitalize()

        print('%s picks up the revolver ...' % name)
        input('Press enter key to pull the trigger ...')

        if revolver.pop():
            print('BANG')
            print('%s had died ...' % name)
            break
        else:
            print('CLICK')
            print('%s lives to see another day ...' % name)

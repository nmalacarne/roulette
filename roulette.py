#!/usr/bin/python3

import random

def is_adding_player():
    return input('Add another player? (y/n) ').lower() == 'y' 

def create_player():
    return {
        'name': input('Enter a name for the player: ')
    }

def create_revolver():
    revolver = [False] * 6
    revolver[random.randint(0, 5)] = 'shell'
    return revolver

